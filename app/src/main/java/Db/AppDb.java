package Db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import Models.Student;

@Database(entities = {Student.class}, version = 1, exportSchema = false)
public abstract class AppDb extends RoomDatabase
{

    //public abstract StudentDao studentDao();

    private static volatile AppDb INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static AppDb getDatabase(final Context context)
    {
        if (INSTANCE == null)
        {
            synchronized (AppDb.class)
            {
                if (INSTANCE == null)
                {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDb.class, "db")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}